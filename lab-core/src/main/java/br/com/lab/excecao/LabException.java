package br.com.lab.excecao;

public class LabException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Construtor.
     *
     * @param mensagem
     */
    public LabException(String mensagem) {
        super(mensagem);
    }

    /**
     * Construtor.
     *
     * @param excecao
     * @param mensagem
     */
    public LabException(Throwable excecao, String mensagem) {
        super(mensagem, excecao);
    }

}
