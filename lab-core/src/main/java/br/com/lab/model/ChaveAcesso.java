package br.com.lab.model;

import java.io.Serializable;

public class ChaveAcesso implements Serializable {

    private static final long serialVersionUID = 1L;

    private String accessToken;
    private String refreshToken;
    private String escopo;
    private String tipoToken;
    private Integer tempoExpiracao;

    /**
     * Construtor.
     *
     * @param accessToken
     * @param refreshToken
     * @param escopo
     * @param tipoToken
     * @param tempoExpiracao
     */
    public ChaveAcesso(String accessToken, String refreshToken, String escopo, String tipoToken,
            Integer tempoExpiracao) {
        super();
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.escopo = escopo;
        this.tipoToken = tipoToken;
        this.tempoExpiracao = tempoExpiracao;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getEscopo() {
        return escopo;
    }

    public void setEscopo(String escopo) {
        this.escopo = escopo;
    }

    public String getTipoToken() {
        return tipoToken;
    }

    public void setTipoToken(String tipoToken) {
        this.tipoToken = tipoToken;
    }

    public Integer getTempoExpiracao() {
        return tempoExpiracao;
    }

    public void setTempoExpiracao(Integer tempoExpiracao) {
        this.tempoExpiracao = tempoExpiracao;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

}
