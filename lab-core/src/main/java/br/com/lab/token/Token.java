package br.com.lab.token;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.HttpHeaders;

import com.google.gson.Gson;

import br.com.lab.excecao.LabException;
import br.com.lab.model.AccessToken;
import br.com.lab.model.ChaveAcesso;
import br.com.lab.util.Constantes;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Token {
	
	public static void main(String[] args) throws LabException {
		Token t = new Token();
		ChaveAcesso c = t.obterChaveAcesso();
		System.out.println("AccessToken: " + c.getAccessToken());
		System.out.println("RefreshToken: " + c.getRefreshToken());
		System.out.println("Escopo: " + c.getEscopo());
		System.out.println("TipoToken: " + c.getTipoToken());
		System.out.println("TempoExpiracao: " + c.getTempoExpiracao());
	}

	private ChaveAcesso obterChaveAcesso() throws LabException {

		AccessToken accessToken;
		Response response = null;
		ChaveAcesso chaveAcesso = null;
		try {
			MediaType mediaType = MediaType.parse(javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED);
			StringBuilder payload = new StringBuilder();
			
			payload.append(Constantes.Autenticacao.APIManager.GRANT_TYPE).append("=").append(Constantes.Autenticacao.APIManager.CLIENT_CREDENTIALS.toLowerCase());

			RequestBody body = RequestBody.create(mediaType, payload.toString());
			URL url = this.obterURLAPIManagerComEndpoint(Constantes.Autenticacao.APIManager.ENDPOINT_TOKEN);
			Request request = this.criarRequestPOST(url, body);

			response = obterOkHttpClient().newCall(request).execute();
			if (response.isSuccessful() && Objects.nonNull(response.body())) {
				accessToken = new Gson().fromJson(response.body().string(), AccessToken.class);
				chaveAcesso = this.carregarChaveAcesso(accessToken);
			} else {
				String msgAlerta = String.format(
						"[IDENTIFICACAO] Nao foi possivel obter Access Token, codigo: %d, mensagem: %s, corpo: %s",
						response.code(), response.message(), response.body() != null ? response.body().toString() : "");
				throw new LabException(msgAlerta);
			}
		} catch (IOException ex) {
			throw new LabException(ex, "[IDENTIFICACAO] Nao foi possivel realizar autenticacao.");
		} finally {
			if (Objects.nonNull(response)) {
				response.close();
			}
		}

		return chaveAcesso;
	}

	private URL obterURLAPIManagerComEndpoint(final String endpoint) throws LabException {

		URL urlCompleta = null;
		String urlApiManager = Constantes.VariavelAmbiente.VAR_AMB_URL_API_MANAGER;
		try {
			if (Objects.nonNull(urlApiManager)) {
				if (!urlApiManager.endsWith("/")) {
					urlCompleta = new URL(String.format("%s/", urlApiManager));
					if (Objects.nonNull(endpoint)) {
						urlCompleta = new URL(urlCompleta.toExternalForm().concat(endpoint));
					}
				}
			} else {
				throw new LabException("Variavel de ambiente nula.");
			}
		} catch (MalformedURLException ex) {
			throw new LabException(ex, "Variavel de ambiente mal formatada.");
		}

		return urlCompleta;
	}

	private Request criarRequestPOST(URL url, RequestBody body) {
		return new Request.Builder().url(url).post(body)
				.addHeader(HttpHeaders.AUTHORIZATION,"Basic ".concat(Constantes.VariavelAmbiente.VAR_AMB_TOKEN_BASIC_API_MANAGER))
				.addHeader(HttpHeaders.CONTENT_TYPE, javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED).build();
	}

	private OkHttpClient obterOkHttpClient() {
		return new OkHttpClient().newBuilder().connectTimeout(5, TimeUnit.MINUTES).readTimeout(5, TimeUnit.MINUTES)
				.retryOnConnectionFailure(true).build();
	}

	private ChaveAcesso carregarChaveAcesso(AccessToken accessToken) {
		return new ChaveAcesso(
			accessToken.getAccessToken(), 
			accessToken.getAccessToken(), 
			accessToken.getScope(),
			accessToken.getTokenType(), 
			accessToken.getExpiresIn()
		);
	}

}
