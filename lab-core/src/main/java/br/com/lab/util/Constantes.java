package br.com.lab.util;


public final class Constantes {

    private Constantes() {

    }

    /**
     * Classe responsavel pelas variaveis de ambiente.
     */
    public static class VariavelAmbiente {
    	
    	private VariavelAmbiente() {
    		// Construtor vazio
		}

        /**
         * Variavel referente ao endereco do API Manager
         */
        public static final String VAR_AMB_URL_API_MANAGER = System.getProperty("tree.url.apimanager");

        /**
         * Variavel referente ao Basic do application do API Manager
         */
        public static final String VAR_AMB_TOKEN_BASIC_API_MANAGER = System.getProperty("tree.token.basic.apimanager");
    }

    /**
     * Classe responsavel pelas constantes de autenticação.
     */
    public static class Autenticacao {
    	
    	private Autenticacao() {
    		// Construtor vazio
		}

        /**
         * Classe responsavel pelas constantes referentes ao API Manager.
         */
        public static class APIManager {
        	
        	private APIManager() {
        		// Construtor vazio
			}

            /**
             * Endpoint token.
             */
            public static final String ENDPOINT_TOKEN = "token";

            /**
             * Grant type.
             */
            public static final String GRANT_TYPE = "grant_type";

            /**
             * Client credentials.
             */
            public static final String CLIENT_CREDENTIALS = "client_credentials";
        }
    }
}
