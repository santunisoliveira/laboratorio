package br.com.lab.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropertiesUtil {

    private static volatile PropertiesUtil instancia;
    private static final String FILE = "/lab.properties";
    private Properties propriedades;

    /**
     * Metodo responsavel por obter a instancia da classe.
     *
     * @return {@link PropertiesUtil}
     */
    public static PropertiesUtil getInstance() {
        if (instancia == null) {
            synchronized (PropertiesUtil.class) {
                instancia = new PropertiesUtil();
            }
        }
        return instancia;
    }

    private PropertiesUtil() {

    }

    /**
     * Metodo responsavel por carregar o arquivo de propriedades.
     */
    private void carregarArquivo() {
        try {
            InputStream input = PropertiesUtil.class.getResourceAsStream(FILE);
            this.propriedades.load(input);
        } catch (IOException excecao) {
        	excecao.getMessage();
        }
    }

    /**
     * Metodo responsavel por obter o valor da propriedade atraves da chave.
     *
     * @param chave a chave que se deseja saber o valor.
     * @return {@link String} contento do valor.
     */
    public String obter(String chave) {
        inicializar();
        return (String) propriedades.get(chave);
    }

    /**
     * Metodo responsavel po inicializar a classe.
     */
    private void inicializar() {
        if (this.propriedades == null) {
            this.propriedades = new Properties();
            carregarArquivo();
        }
    }
}
